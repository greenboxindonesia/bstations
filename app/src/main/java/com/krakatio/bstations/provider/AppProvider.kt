package com.krakatio.bstations.provider

import android.content.Context
import com.krakatio.bstations.data.local.AppDatabase

object AppProvider {

    fun appDatabase(context: Context): AppDatabase {
        return AppDatabase.getInstance(context)
    }

}