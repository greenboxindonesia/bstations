package com.krakatio.bstations.provider.repository

import android.content.Context
import com.krakatio.bstations.data.local.source.PlaceLocalDataSource
import com.krakatio.bstations.domain.source.PlaceDataSource
import com.krakatio.bstations.provider.AppProvider

object DataSourceProvider {
    object Local {
        fun place(context: Context): PlaceDataSource {
            val appDb = AppProvider.appDatabase(context)
            return PlaceLocalDataSource.getInstance(appDb.placeDao())
        }
    }
}