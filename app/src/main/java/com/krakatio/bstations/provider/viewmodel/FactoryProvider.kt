package com.krakatio.bstations.provider.viewmodel

import android.content.Context
import com.krakatio.bstations.provider.repository.RepositoryProvider
import com.krakatio.bstations.vm.PlaceViewModel

object FactoryProvider {

    fun place(context: Context): PlaceViewModel.Factory {
        return PlaceViewModel.Factory(RepositoryProvider.place(context))
    }

}