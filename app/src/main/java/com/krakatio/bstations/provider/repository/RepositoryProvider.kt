package com.krakatio.bstations.provider.repository

import android.content.Context
import com.krakatio.bstations.data.repository.PlaceRepositoryImpl
import com.krakatio.bstations.domain.source.PlaceDataSource

object RepositoryProvider {

    fun place(context: Context): PlaceDataSource {
        val local = DataSourceProvider.Local.place(context)
        return PlaceRepositoryImpl.getInstance(local)
    }

}