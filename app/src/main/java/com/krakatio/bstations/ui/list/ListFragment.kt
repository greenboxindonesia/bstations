package com.krakatio.bstations.ui.list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.FragmentListBinding
import com.krakatio.bstations.domain.model.Place
import com.krakatio.bstations.provider.viewmodel.FactoryProvider
import com.krakatio.bstations.vm.PlaceViewModel


class ListFragment : Fragment() {

    private lateinit var binding: FragmentListBinding
    private val viewModel: PlaceViewModel by activityViewModels {
        FactoryProvider.place(requireContext())
    }

    private val placeAdapter = PlaceListAdapter { openDetailPlacePage(it) }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_list, container, false
        )
        binding.view = this
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        binding.listPlace.adapter = placeAdapter

        viewModel.placeByCategoryListObservable.observe(viewLifecycleOwner, Observer { list ->
            placeAdapter.submitList(list.map { it.first })
        })

        return binding.root
    }


    fun closePage() {
        findNavController().navigateUp()
    }

    fun openFilterPage() {
        findNavController().navigate(R.id.action_listFragment_to_filterFragment)
    }

    fun openExplorePage() {
        findNavController().navigate(R.id.action_listFragment_to_exploreFragment)
    }

    private fun openDetailPlacePage(place: Place) {
        viewModel.setDetailPlace(place)
        findNavController().navigate(R.id.action_listFragment_to_placeFragment)
    }


}