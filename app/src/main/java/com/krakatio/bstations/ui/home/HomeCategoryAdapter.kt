package com.krakatio.bstations.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.ItemCategoryBinding
import com.krakatio.bstations.domain.model.Category
import com.krakatio.bstations.util.GeneralDiffUtilCallback

class HomeCategoryAdapter(
    private val onItemClick: (category: Category) -> Unit
) : ListAdapter<Pair<Category, String>, HomeCategoryAdapter.ViewHolder>(GeneralDiffUtilCallback<Pair<Category, String>>()) {

    inner class ViewHolder(val binding: ItemCategoryBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemCategoryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_category,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.category = item.first
        holder.binding.subtitle = item.second
        holder.binding.imageCategory.setImageResource(item.first.imageRes)
        holder.binding.cardItem.setOnClickListener { onItemClick(item.first) }
    }
}