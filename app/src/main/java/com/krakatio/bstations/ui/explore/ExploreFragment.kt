package com.krakatio.bstations.ui.explore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.FragmentExploreBinding
import com.krakatio.bstations.domain.model.Place
import com.krakatio.bstations.provider.viewmodel.FactoryProvider
import com.krakatio.bstations.util.hideOrShowView
import com.krakatio.bstations.vm.PlaceViewModel


class ExploreFragment : Fragment() {

    private lateinit var binding: FragmentExploreBinding
    private lateinit var gMap: GoogleMap
    private val viewModel: PlaceViewModel by activityViewModels {
        FactoryProvider.place(requireContext())
    }

    private var isMapPrepared: Boolean = false
    private var previousSelectedPlace: Place? = null
    private val markerList = arrayListOf<Marker>()

    private val exploreAdapter = ExploreAdapter(
        onItemClick = { showMarker(it) },
        onDetailClick = { openDetailPlacePage(it) }
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_explore, container, false
        )
        binding.view = this
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        binding.listPlace.adapter = exploreAdapter

        viewModel.placeByCategoryListObservable.observe(viewLifecycleOwner, Observer { list ->
            exploreAdapter.submitList(list)
            if (isMapPrepared) prepareMarkers(list)
            else {
                binding.mapView.apply {
                    onCreate(savedInstanceState)
                    onResume()
                    MapsInitializer.initialize(requireContext().applicationContext)
                    getMapAsync {
                        gMap = it
                        setupMapView()
                        prepareMarkers(list)
                    }
                }
            }
        })

        return binding.root
    }


    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding.mapView.onLowMemory()
    }

    override fun onPause() {
        binding.mapView.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        binding.mapView.onDestroy()
        super.onDestroy()
    }

    fun closePage() {
        findNavController().navigateUp()
    }

    fun openFilterPage() {
        findNavController().navigate(R.id.action_exploreFragment_to_filterFragment)
    }

    fun openListPage() {
        findNavController().navigateUp()
    }

    private fun openDetailPlacePage(place: Place) {
        viewModel.setDetailPlace(place)
        findNavController().navigate(R.id.action_exploreFragment_to_placeFragment)
    }

    private fun setupMapView() {
        gMap.clear()
        gMap.uiSettings.isMapToolbarEnabled = false
        gMap.setOnCameraMoveStartedListener { binding.groupActions.hideOrShowView(isShown = false) }
        gMap.setOnCameraIdleListener { binding.groupActions.hideOrShowView(isShown = true) }
        gMap.setOnMarkerClickListener { marker ->
            val index = markerList.indexOfFirst { it.tag == marker.tag }
            if (index != -1) {
                binding.listPlace.smoothScrollToPosition(index)
            }
            index != -1
        }
        isMapPrepared = true
    }

    private fun prepareMarkers(list: List<Pair<Place, Boolean>>) {
        gMap.clear()
        markerList.clear()
        val latLngBuilder = LatLngBounds.builder()
        for (item in list) {
            val place = item.first
            val latLong = LatLng(place.latitude, place.longitude)
            val marker = gMap.addMarker(
                MarkerOptions()
                    .position(latLong)
                    .title(place.name)
            )
            marker?.tag = place.id
            markerList.add(marker!!)
            latLngBuilder.include(latLong)
        }
        if (list.isNotEmpty()) {
            gMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBuilder.build(), 32))
        }
    }

    fun showMarker(position: Int) {
        val marker = markerList[position]
        val cameraPosition = CameraPosition.builder()
            .target(marker.position)
            .zoom(19f)
            .bearing(0f)
            .tilt(45f)
            .build()
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }



}