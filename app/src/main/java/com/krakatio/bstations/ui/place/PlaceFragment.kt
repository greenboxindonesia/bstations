package com.krakatio.bstations.ui.place

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.vr.sdk.widgets.pano.VrPanoramaView
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.FragmentPlaceBinding
import com.krakatio.bstations.domain.model.Place
import com.krakatio.bstations.provider.viewmodel.FactoryProvider
import com.krakatio.bstations.util.hideOrShowView
import com.krakatio.bstations.vm.PlaceViewModel


class PlaceFragment : Fragment() {

    private lateinit var binding: FragmentPlaceBinding
    private lateinit var gMap: GoogleMap
    private val viewModel: PlaceViewModel by activityViewModels {
        FactoryProvider.place(requireContext())
    }

    private val similarPlaceAdapter = SimilarPlaceAdapter {
        viewModel.setDetailPlace(it)
        binding.scrollable.smoothScrollTo(0, 0)
    }

    private var currentPlace: Place? = null
    private var isMapPrepared: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_place, container, false
        )
        binding.view = this
        binding.lifecycleOwner = viewLifecycleOwner
        binding.listSimilarPlace.adapter = similarPlaceAdapter
        binding.panoramaView.setInfoButtonEnabled(false)
        binding.panoramaView.setPureTouchTracking(true)

        viewModel.detailPlaceObservable.observe(viewLifecycleOwner, Observer { place ->
            binding.progressBar.hideOrShowView(isShown = true)
            currentPlace = place
            binding.place = place
            Glide.with(this).asBitmap()
                .load(place.imageUrl)
                .into(object : CustomTarget<Bitmap>() {
                    override fun onLoadCleared(placeholder: Drawable?) {}
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap>?
                    ) {
                        val options = VrPanoramaView.Options()
                        options.inputType = VrPanoramaView.Options.TYPE_MONO
                        binding.panoramaView.loadImageFromBitmap(resource, options)
                        binding.progressBar.hideOrShowView(isShown = false)
                    }
                })
            if (isMapPrepared) {
                val latLong = LatLng(place.latitude, place.longitude)
                gMap.addMarker(
                    MarkerOptions()
                        .position(latLong)
                )
                val cameraPosition = CameraPosition.builder()
                    .target(latLong)
                    .zoom(19f)
                    .bearing(0f)
                    .tilt(45f)
                    .build()
                gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            } else {
                binding.mapView.apply {
                    onCreate(savedInstanceState)
                    onResume()
                    MapsInitializer.initialize(requireContext().applicationContext)
                    getMapAsync {
                        gMap = it
                        setupMapView()
                        val latLong = LatLng(place.latitude, place.longitude)
                        gMap.addMarker(
                            MarkerOptions()
                                .position(latLong)
                        )
                        val cameraPosition = CameraPosition.builder()
                            .target(latLong)
                            .zoom(19f)
                            .bearing(0f)
                            .tilt(45f)
                            .build()
                        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                    }
                }
            }
        })
        viewModel.placeByCategoryListObservable.observe(viewLifecycleOwner, Observer { list ->
            similarPlaceAdapter.submitList(list.map { it.first }
                .filter { it.id != currentPlace?.id })
        })

        return binding.root
    }


    override fun onResume() {
        binding.mapView.onResume()
        binding.panoramaView.resumeRendering()
        super.onResume()
    }

    override fun onLowMemory() {
        binding.mapView.onLowMemory()
        super.onLowMemory()
    }

    override fun onPause() {
        binding.mapView.onPause()
        binding.panoramaView.pauseRendering()
        super.onPause()
    }

    override fun onDestroy() {
        binding.mapView.onDestroy()
        binding.panoramaView.shutdown()
        super.onDestroy()
    }

    fun closePage() {
        findNavController().navigateUp()
    }

    fun openGoogleMapsApp() {
        val place = currentPlace
        if (place == null) {
            Toast.makeText(requireContext(), "Tidak ada destinasi yg dipilih.", Toast.LENGTH_LONG)
                .show()
        } else {
            try {
                val gmmIntentUri = Uri.parse("google.navigation:q=${place.latitude},${place.longitude}")
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)
            } catch (ex: Exception) {
                Toast.makeText(requireContext(), "Tidak terdapat google maps.", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun setupMapView() {
        gMap.clear()
        gMap.uiSettings.isMapToolbarEnabled = false
        isMapPrepared = true
    }


}