package com.krakatio.bstations.ui.place

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.maps.GoogleMap
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.ActivityPlaceBinding
import com.krakatio.bstations.domain.model.base.Event
import timber.log.Timber

class PlaceActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPlaceBinding
    private lateinit var gMap: GoogleMap

    private val sampleLatitude = -7.9701115
    private val sampleLongitude = 112.6362534

    private val panoramaBitmapObservable = MutableLiveData<Event<Bitmap>>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_place)
        binding.lifecycleOwner = this
        Glide.with(this).asBitmap().load("").into(object : CustomTarget<Bitmap>() {
            override fun onLoadCleared(placeholder: Drawable?) {}
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
//                binding.panoramaView.setPanorama(resource, false)
                Toast.makeText(this@PlaceActivity, "loaded", Toast.LENGTH_SHORT).show()
                Timber.d("load: ${resource.width}, ${resource.height}")
            }
        })

//        lifecycleScope.launch(Dispatchers.IO) {
//            val assets = assets
//            try {
//                val stream = assets.open("sample_alun_alun.jpg")
//                val bitmap = BitmapFactory.decodeResource(resources, R.drawable.preview_panorama)//BitmapFactory.decodeStream(stream)
//                panoramaBitmapObservable.postValue(Event(bitmap))
//            } catch (ex: Exception) {
//                Timber.e(ex)
//            }
//        }

//        binding.panoramaView.setPureTouchTracking(true)
//        binding.panoramaView.setFlingingEnabled(true)
//        panoramaBitmapObservable.observe(this, Observer { event ->
//            event.getAvailableEvent()?.let {
//                val options = VrPanoramaView.Options()
//                options.inputType = VrPanoramaView.Options.TYPE_MONO
//                binding.panoramaView.loadImageFromBitmap(it, options)
//            }
//        })

//        binding.mapView.apply {
//            onCreate(savedInstanceState)
//            onResume()
//            MapsInitializer.initialize(applicationContext)
//            getMapAsync {
//                gMap = it
//                setupMapView()
//            }
//        }
    }
}