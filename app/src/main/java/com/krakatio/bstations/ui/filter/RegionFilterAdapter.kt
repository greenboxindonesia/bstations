package com.krakatio.bstations.ui.filter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.ItemFilterRegionBinding
import com.krakatio.bstations.domain.model.Region
import com.krakatio.bstations.util.GeneralDiffUtilCallback

class RegionFilterAdapter(
    private val onItemClick: (region: Region) -> Unit
) : ListAdapter<Pair<Region, Boolean>, RegionFilterAdapter.ViewHolder>(GeneralDiffUtilCallback<Pair<Region, Boolean>>()) {

    inner class ViewHolder(val binding: ItemFilterRegionBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemFilterRegionBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_filter_region,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.region = item.first
        holder.binding.isIncluded = item.second
        holder.binding.checkbox.setOnClickListener { onItemClick(item.first) }
    }

}