package com.krakatio.bstations.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.ItemPlaceVerticalBinding
import com.krakatio.bstations.domain.model.Place
import com.krakatio.bstations.util.ModelDiffUtilCallback

class PlaceListAdapter(
    private val onItemClick: (place: Place) -> Unit
) : ListAdapter<Place, PlaceListAdapter.ViewHolder>(ModelDiffUtilCallback<Place>()) {

    inner class ViewHolder(val binding: ItemPlaceVerticalBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemPlaceVerticalBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_place_vertical,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.place = item
        holder.binding.cardItem.setOnClickListener { onItemClick(item) }
    }
}