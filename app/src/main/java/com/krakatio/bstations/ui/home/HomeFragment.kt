package com.krakatio.bstations.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.FragmentHomeBinding
import com.krakatio.bstations.domain.model.Category
import com.krakatio.bstations.provider.viewmodel.FactoryProvider
import com.krakatio.bstations.vm.PlaceViewModel


class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private val viewModel: PlaceViewModel by activityViewModels {
        FactoryProvider.place(requireContext())
    }

    private val categoryAdapter = HomeCategoryAdapter { openListPage(it) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_home, container, false
        )
        binding.view = this
        binding.lifecycleOwner = viewLifecycleOwner
        binding.listCategory.adapter = categoryAdapter

        viewModel.categoryListObservable.observe(viewLifecycleOwner){
            categoryAdapter.submitList(it)
        }


//            viewLifecycleOwner,
//            Observer { categoryAdapter.submitList(it) })

        return binding.root
    }

    fun openSearchPage() {
        viewModel.selectCategory(Category.ALL)
        findNavController().navigate(R.id.action_homeFragment_to_listFragment)
    }

    fun openProfilePage() {
        findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
    }

    private fun openListPage(category: Category) {
        viewModel.selectCategory(category)
        findNavController().navigate(R.id.action_homeFragment_to_listFragment)
    }


}