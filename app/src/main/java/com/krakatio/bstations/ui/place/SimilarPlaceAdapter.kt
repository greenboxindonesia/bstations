package com.krakatio.bstations.ui.place

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.ItemPlaceSimilarBinding
import com.krakatio.bstations.domain.model.Place
import com.krakatio.bstations.util.ModelDiffUtilCallback

class SimilarPlaceAdapter(
    private val onItemClick: (place: Place) -> Unit
) : ListAdapter<Place, SimilarPlaceAdapter.ViewHolder>(ModelDiffUtilCallback<Place>()) {

    inner class ViewHolder(val binding: ItemPlaceSimilarBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemPlaceSimilarBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_place_similar,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.place = item
        holder.binding.cardItem.setOnClickListener { onItemClick(item) }
    }
}