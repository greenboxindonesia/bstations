package com.krakatio.bstations.ui.explore

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.ItemPlaceHorizontalBinding
import com.krakatio.bstations.domain.model.Place
import com.krakatio.bstations.util.GeneralDiffUtilCallback

class ExploreAdapter (
    private val onItemClick: (position: Int) -> Unit,
    private val onDetailClick: (place: Place) -> Unit
) : ListAdapter<Pair<Place, Boolean>, ExploreAdapter.ViewHolder>(GeneralDiffUtilCallback<Pair<Place, Boolean>>()) {

    inner class ViewHolder(val binding: ItemPlaceHorizontalBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemPlaceHorizontalBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_place_horizontal,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.place = item.first
        holder.binding.isSelected = item.second
        holder.binding.cardItem.setOnClickListener { onItemClick(position) }
        holder.binding.buttonDetails.setOnClickListener { onDetailClick(item.first) }
    }
}