package com.krakatio.bstations.ui.filter

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.krakatio.bstations.R
import com.krakatio.bstations.databinding.FragmentFilterBinding
import com.krakatio.bstations.provider.viewmodel.FactoryProvider
import com.krakatio.bstations.vm.PlaceViewModel


class FilterFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentFilterBinding
    private val viewModel: PlaceViewModel by activityViewModels {
        FactoryProvider.place(requireContext())
    }

    private val filterAdapter = RegionFilterAdapter { viewModel.setRegionFilter(it) }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_filter, container, false
        )
        binding.view = this
        binding.lifecycleOwner = viewLifecycleOwner
        binding.listRegion.adapter = filterAdapter

        viewModel.regionFilterListObservable.observe(
            viewLifecycleOwner,
            Observer { filterAdapter.submitList(it) })

        return binding.root
    }

    fun closePage() {
        findNavController().navigateUp()
    }

}