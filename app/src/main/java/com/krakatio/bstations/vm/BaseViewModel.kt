package com.krakatio.bstations.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.krakatio.bstations.domain.model.base.Event

open class BaseViewModel : ViewModel() {

    protected val _loadingIndicator = MediatorLiveData<Boolean>()
    val loadingIndicator: LiveData<Boolean>
        get() = _loadingIndicator

    protected val _eventMessage = MutableLiveData<Event<String>>()
    val eventMessage: LiveData<Event<String>>
        get() = _eventMessage

    init {
        _loadingIndicator.value = false
    }

}