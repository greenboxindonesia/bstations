package com.krakatio.bstations.vm

import androidx.lifecycle.*
import androidx.lifecycle.viewmodel.CreationExtras
import com.krakatio.bstations.domain.model.Category
import com.krakatio.bstations.domain.model.Place
import com.krakatio.bstations.domain.model.Region
import com.krakatio.bstations.domain.source.PlaceDataSource
import com.krakatio.bstations.util.default

class PlaceViewModel private constructor(
    private val placeDataSource: PlaceDataSource
) : BaseViewModel() {

    val homeSearchQuery = MutableLiveData<String>()
    val listSearchQuery = MutableLiveData<String>()

    private val placeList = arrayListOf<Place>()
    private val _placeListObservable = MediatorLiveData<List<Place>>()
    val placeListObservable: LiveData<List<Place>>
        get() = _placeListObservable

    val categoryListObservable: LiveData<List<Pair<Category, String>>>
        get() = placeListObservable.map { places ->
            places.groupBy { it.category }
                .filter { it.key != Category.ALL }
                .map { Pair(it.key, it.key.subtitle(it.value.size)) }
        }

    private val regionFilterList = arrayListOf<Pair<Region, Boolean>>().apply {
        addAll(Region.all().map { Pair(it, true) })
    }
    private val _regionFilterListObservable =
        MutableLiveData<List<Pair<Region, Boolean>>>().default(initialValue = regionFilterList)
    val regionFilterListObservable: LiveData<List<Pair<Region, Boolean>>>
        get() = _regionFilterListObservable

    private var selectedCategory: Category? = null
    private val _selectedCategoryObservable = MutableLiveData<Category>()
    val selectedCategoryObservable: LiveData<Category>
        get() = _selectedCategoryObservable

    private val _placeByCategoryListObservable = MediatorLiveData<List<Pair<Place, Boolean>>>()
    val placeByCategoryListObservable: LiveData<List<Pair<Place, Boolean>>>
        get() = _placeByCategoryListObservable

    private val _selectedPlaceInMapsObservable = MutableLiveData<Place>()
    val selectedPlaceInMapsObservable: LiveData<Place>
        get() = _selectedPlaceInMapsObservable

    private val _detailPlaceObservable = MutableLiveData<Place>()
    val detailPlaceObservable: LiveData<Place>
        get() = _detailPlaceObservable

    init {
        _regionFilterListObservable.value = regionFilterList
        _placeListObservable.apply {
            addSource(placeDataSource.placeListObservable) {
                placeList.clear()
                placeList.addAll(it)
                value = it
            }
        }

        _placeByCategoryListObservable.apply {
            addSource(placeListObservable) { list ->
                val query = listSearchQuery.value ?: ""
                val selectedPlace = selectedPlaceInMapsObservable.value
                val newList = arrayListOf<Pair<Place, Boolean>>()
                for (place in list) {
                    var isRegion = false
                    for (item in regionFilterList) {
                        if (place.region == item.first && item.second) isRegion = true
                    }
                    if ((selectedCategory == Category.ALL || place.category == selectedCategory) && place.contains(
                            query
                        ) && isRegion
                    ) {
                        newList.add(Pair(place, place.id == selectedPlace?.id))
                    }
                }
                value = newList
            }
            addSource(selectedCategoryObservable) { category ->
                val query = listSearchQuery.value ?: ""
                val selectedPlace = selectedPlaceInMapsObservable.value
                val newList = arrayListOf<Pair<Place, Boolean>>()
                for (place in placeList) {
                    var isRegion = false
                    for (item in regionFilterList) {
                        if (place.region == item.first && item.second) isRegion = true
                    }
                    if ((category == Category.ALL || place.category == category) && place.contains(
                            query
                        ) && isRegion
                    ) {
                        newList.add(Pair(place, place.id == selectedPlace?.id))
                    }
                }
                value = newList
            }
            addSource(listSearchQuery) { query ->
                val selectedPlace = selectedPlaceInMapsObservable.value
                val newList = arrayListOf<Pair<Place, Boolean>>()
                for (place in placeList) {
                    var isRegion = false
                    for (item in regionFilterList) {
                        if (place.region == item.first && item.second) isRegion = true
                    }
                    if ((selectedCategory == Category.ALL || place.category == selectedCategory) && place.contains(
                            query
                        ) && isRegion
                    ) {
                        newList.add(Pair(place, place.id == selectedPlace?.id))
                    }
                }
                value = newList
            }
            addSource(regionFilterListObservable) { filters ->
                val query = listSearchQuery.value ?: ""
                val selectedPlace = selectedPlaceInMapsObservable.value
                val newList = arrayListOf<Pair<Place, Boolean>>()
                for (place in placeList) {
                    var isRegion = false
                    for (item in filters) {
                        if (place.region == item.first && item.second) isRegion = true
                    }
                    if ((selectedCategory == Category.ALL || place.category == selectedCategory) && place.contains(
                            query
                        ) && isRegion
                    ) {
                        newList.add(Pair(place, place.id == selectedPlace?.id))
                    }
                }
                value = newList
            }
        }
    }

    fun selectCategory(category: Category) {
        selectedCategory = category
        _selectedCategoryObservable.value = category
        clearListFilter()
    }

    fun setRegionFilter(region: Region) {
        val list = arrayListOf<Pair<Region, Boolean>>()
        for (filter in regionFilterList) {
            if (filter.first == region) {
                list.add(Pair(filter.first, !filter.second))
            } else {
                list.add(filter)
            }
        }
        regionFilterList.clear()
        regionFilterList.addAll(list)
        _regionFilterListObservable.value = regionFilterList
    }

    fun setSelectedPlaceInMap(place: Place) {
        placeByCategoryListObservable.value?.let { list ->
            val newList = arrayListOf<Pair<Place, Boolean>>()
            for (item in list) {
                if (item.first.id == place.id) {
                    newList.add(Pair(place, true))
                } else {
                    newList.add(item)
                }
            }
            _placeByCategoryListObservable.value = newList
        }
    }

    fun setDetailPlace(place: Place) {
        _detailPlaceObservable.value = place
    }

    fun clearListFilter() {
        listSearchQuery.value = ""
        _regionFilterListObservable.value = Region.all().map { Pair(it, true) }

        val newList = arrayListOf<Pair<Place, Boolean>>()
        for (item in placeList) newList.add(Pair(item, false))
        _placeByCategoryListObservable.value = newList
    }

    class Factory(private val placeDataSource: PlaceDataSource) :
        ViewModelProvider.NewInstanceFactory() {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
            return PlaceViewModel(placeDataSource) as T
        }
    }

}