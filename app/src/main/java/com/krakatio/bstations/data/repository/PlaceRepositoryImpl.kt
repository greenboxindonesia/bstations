package com.krakatio.bstations.data.repository

import androidx.lifecycle.LiveData
import com.krakatio.bstations.domain.model.Place
import com.krakatio.bstations.domain.source.PlaceDataSource

class PlaceRepositoryImpl private constructor(
    private val local: PlaceDataSource
) : PlaceDataSource {

    companion object {
        @Volatile
        private var instance: PlaceDataSource? = null
        fun getInstance(local: PlaceDataSource): PlaceDataSource {
            return instance ?: synchronized(this) {
                instance ?: PlaceRepositoryImpl(local).also { instance = it }
            }
        }
    }

    override val placeListObservable: LiveData<List<Place>>
        get() = local.placeListObservable

    override suspend fun getPlaceList(): List<Place> {
        return local.getPlaceList()
    }

}