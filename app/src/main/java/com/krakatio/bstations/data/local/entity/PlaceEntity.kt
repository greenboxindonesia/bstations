package com.krakatio.bstations.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "places")
data class PlaceEntity(
    @PrimaryKey var id: Long,
    var name: String,
    var category: String,
    var region: String,
    var address: String,
    var description: String,
    var latitude: Double,
    var longitude: Double,
    var imageUrl: String
)