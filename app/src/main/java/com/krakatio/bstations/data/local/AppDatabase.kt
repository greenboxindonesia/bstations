package com.krakatio.bstations.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.krakatio.bstations.BuildConfig
import com.krakatio.bstations.data.local.dao.PlaceDao
import com.krakatio.bstations.data.local.entity.PlaceEntity

@Database(
    version = BuildConfig.DB_VERSION,
    exportSchema = false,
    entities = [PlaceEntity::class]
)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        @Volatile
        private var sInstance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return sInstance ?: synchronized(this) {
                sInstance ?: buildDatabase(context).also { sInstance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                BuildConfig.DB_NAME
            ).createFromAsset("krakatio_sipadu_default.db")
                .fallbackToDestructiveMigration()
                .build()
        }
    }

    abstract fun placeDao(): PlaceDao

}