package com.krakatio.bstations.data.local.source

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.krakatio.bstations.data.local.dao.PlaceDao
import com.krakatio.bstations.data.local.mapper.LocalPlaceMapper
import com.krakatio.bstations.domain.model.Place
import com.krakatio.bstations.domain.source.PlaceDataSource

class PlaceLocalDataSource private constructor(
    private val placeDao: PlaceDao
) : PlaceDataSource {

    companion object {
        @Volatile
        private var instance: PlaceDataSource? = null
        fun getInstance(placeDao: PlaceDao): PlaceDataSource {
            return instance ?: synchronized(this) {
                instance ?: PlaceLocalDataSource(placeDao).also { instance = it }
            }
        }
    }

    override val placeListObservable: LiveData<List<Place>>
        get() = placeDao.getPlaceListObservable().map { LocalPlaceMapper.toDomainList(it) }

    override suspend fun getPlaceList(): List<Place> {
        return placeDao.getPlaceList().map { LocalPlaceMapper.toDomain(it) }
    }
}