package com.krakatio.bstations.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.krakatio.bstations.data.local.entity.PlaceEntity

@Dao
interface PlaceDao : BaseDao<PlaceEntity> {

    @Query(value = "SELECT * FROM places")
    fun getPlaceListObservable(): LiveData<List<PlaceEntity>>

    @Query(value = "SELECT * FROM places")
    suspend fun getPlaceList(): List<PlaceEntity>

}