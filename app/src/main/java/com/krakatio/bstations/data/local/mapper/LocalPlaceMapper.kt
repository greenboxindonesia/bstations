package com.krakatio.bstations.data.local.mapper

import com.krakatio.bstations.data.local.entity.PlaceEntity
import com.krakatio.bstations.domain.model.Category
import com.krakatio.bstations.domain.model.Place
import com.krakatio.bstations.domain.model.Region
import com.krakatio.bstations.domain.model.base.Mapper

object LocalPlaceMapper : Mapper<Place, PlaceEntity> {
    override fun toDomain(model: PlaceEntity): Place {
        return Place(
            id = model.id,
            name = model.name,
            category = Category.of(model.category),
            region = Region.of(model.region),
            address = model.address,
            description = model.description,
            latitude = model.latitude,
            longitude = model.longitude,
            imageUrl = model.imageUrl
        )
    }

    override fun fromDomain(model: Place): PlaceEntity {
        return PlaceEntity(
            id = model.id,
            name = model.name,
            category = model.category.text,
            region = model.region.text,
            address = model.address,
            description = model.description,
            latitude = model.latitude,
            longitude = model.longitude,
            imageUrl = model.imageUrl
        )
    }
}