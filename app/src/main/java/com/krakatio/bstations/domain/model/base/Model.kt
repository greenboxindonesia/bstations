package com.krakatio.bstations.domain.model.base

interface Model {
    fun contains(query: String): Boolean = false
    fun areItemSameWith(other: Model): Boolean = false
    fun areContentSameWith(other: Model): Boolean = false
}