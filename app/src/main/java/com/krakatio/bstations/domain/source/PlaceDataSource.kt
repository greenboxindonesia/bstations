package com.krakatio.bstations.domain.source

import androidx.lifecycle.LiveData
import com.krakatio.bstations.domain.model.Place

interface PlaceDataSource {
    val placeListObservable: LiveData<List<Place>>
    suspend fun getPlaceList(): List<Place>
}