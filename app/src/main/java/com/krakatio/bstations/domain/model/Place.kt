package com.krakatio.bstations.domain.model

import com.krakatio.bstations.domain.model.base.Model

data class Place(
    val id: Long,
    val name: String,
    val category: Category,
    val region: Region,
    val address: String,
    val description: String,
    val latitude: Double,
    val longitude: Double,
    val imageUrl: String
) : Model {
    override fun contains(query: String): Boolean {
        return name.contains(query, ignoreCase = true) ||
                category.text.contains(query, ignoreCase = true) ||
                region.text.contains(query, ignoreCase = true) ||
                address.contains(query, ignoreCase = true)
    }
}