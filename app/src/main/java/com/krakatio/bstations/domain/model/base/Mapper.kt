package com.krakatio.bstations.domain.model.base

interface Mapper<M : Model, T> {
    fun toDomain(model: T): M
    fun fromDomain(model: M): T
    fun toDomainList(models: List<T>): List<M> = models.map { toDomain(it) }
    fun fromDomainList(models: List<M>): List<T> = models.map { fromDomain(it) }
}