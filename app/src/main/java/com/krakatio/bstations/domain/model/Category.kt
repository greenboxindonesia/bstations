package com.krakatio.bstations.domain.model

import com.krakatio.bstations.R

enum class Category(val text: String, val imageRes: Int) {
    TOURISM("Wisata", R.drawable.ic_destination_40dp),
    HOTEL("Hotel", R.drawable.ic_acommodation_40dp),
    RESTAURANT("Restoran", R.drawable.ic_ammenity_40dp),
    MOSQUE("Masjid", R.drawable.ic_baseline_mosque_40),
    ALL("Semua", R.drawable.ic_android_black_40dp), ;

    fun subtitle(count: Int): String {
        return "$count+ $text di Pulau Bali."
    }

    companion object {
        fun all(): List<Category> = listOf(TOURISM, HOTEL, RESTAURANT, MOSQUE)

        fun of(text: String): Category {
            return when (text) {
                TOURISM.text -> TOURISM
                HOTEL.text -> HOTEL
                RESTAURANT.text -> RESTAURANT
                MOSQUE.text -> MOSQUE
                else -> ALL
            }
        }
    }
}