package com.krakatio.bstations.domain.model

enum class Region(val text: String) {
    BADUNG(text = "Badung"),
    BULELENG(text = "Buleleng"),
    KARANGASEM(text = "Karangasem"),
    GIANYAR(text = "Gianyar"),
    DENPASAR(text = "Denpasar"),
    JEMBRANA(text = "Jembrana"),
    KLUNGKUNG(text = "Klungkung"),
    TABANAN(text = "Tabanan"),
    BANGLI(text = "Bangli");

    companion object {
        fun all(): List<Region> = listOf(
            BADUNG,
            BULELENG,
            KARANGASEM,
            GIANYAR,
            DENPASAR,
            JEMBRANA,
            KLUNGKUNG,
            TABANAN,
            BANGLI
        )

        fun of(text: String): Region {
            return when (text) {
                BADUNG.text -> BADUNG
                BANGLI.text -> BANGLI
                KARANGASEM.text -> KARANGASEM
                GIANYAR.text -> GIANYAR
                DENPASAR.text -> DENPASAR
                JEMBRANA.text -> JEMBRANA
                KLUNGKUNG.text -> KLUNGKUNG
                TABANAN.text -> TABANAN
                BULELENG.text -> BULELENG
                else -> BULELENG
            }
        }
    }
}