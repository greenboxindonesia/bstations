package com.krakatio.bstations.util

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.krakatio.bstations.R


@BindingAdapter("isShown")
fun View.hideOrShowView(isShown: Boolean) {
    visibility = if (isShown) View.VISIBLE else View.GONE
}

@BindingAdapter(
    value = ["srcRounded", "srcRadius"],
    requireAll = true
)
fun ImageView.setRoundedImageResource(srcRounded: Drawable, srcRadius: Float) {
    Glide.with(this)
        .load(srcRounded)
        .transform(CenterCrop(), RoundedCorners(srcRadius.toInt()))
        .dontAnimate()
        .into(this)
}

@BindingAdapter(
    value = ["srcRemoteRounded", "srcRemoteRadius"],
    requireAll = true
)
fun ImageView.setRoundedImageRemote(srcRemoteRounded: String, srcRemoteRadius: Float) {
    Glide.with(this)
        .load(srcRemoteRounded)
        .transform(CenterCrop(), RoundedCorners(srcRemoteRadius.toInt()))
        .placeholder(R.drawable.ic_outline_image_24dp)
        .error(R.drawable.ic_outline_image_24dp)
        .dontAnimate()
        .into(this)
}

@BindingAdapter(
    value = ["srcRemoteTopRounded", "srcRemoteTopRadius", "srcPlaceholder"],
    requireAll = true
)
fun ImageView.setTopRoundedImageRemote(
    srcRemoteRounded: String,
    srcRemoteRadius: Float,
    srcPlaceholder: Drawable
) {
    Glide.with(this)
        .load(srcRemoteRounded)
        .transform(
            CenterCrop(),
            GranularRoundedCorners(
                srcRemoteRadius,
                srcRemoteRadius,
                0f,
                0f
            )
        )
        .placeholder(srcPlaceholder)
        .error(srcPlaceholder)
        .dontAnimate()
        .into(this)
}