package com.krakatio.bstations.util

import android.app.DatePickerDialog
import android.graphics.drawable.Drawable
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.google.android.material.card.MaterialCardView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textview.MaterialTextView
import com.krakatio.bstations.R
import java.util.*


// LiveData
fun <T : Any?> MutableLiveData<T>.default(initialValue: T) = apply { setValue(initialValue) }

// Material TextView
fun MaterialTextView.startDrawable(@DrawableRes drawableRes: Int) {
    setCompoundDrawablesRelativeWithIntrinsicBounds(drawableRes, 0, 0, 0)
}

fun MaterialTextView.startDrawable(drawable: Drawable?) {
    setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, null, null, null)
}

fun MaterialTextView.endDrawable(@DrawableRes drawableRes: Int) {
    setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, drawableRes, 0)
}

// Material Card View
fun MaterialCardView.setCardBackgroundColorFromResources(@ColorRes colorRes: Int) {
    val color = ResourcesCompat.getColor(resources, colorRes, context.theme)
    setCardBackgroundColor(color)
}

// Fragment
fun Fragment.buildLoadingDialog(): AlertDialog {
    return MaterialAlertDialogBuilder(requireContext())
        .setView(R.layout.dialog_loading)
        .setCancelable(false)
        .create()
}

fun Fragment.showConfirmationDialog(message: String, onConfirm: () -> Unit) {
    MaterialAlertDialogBuilder(requireContext())
        .setMessage(message)
        .setPositiveButton(R.string.action_yes) { _, _ -> onConfirm() }
        .setNegativeButton(R.string.action_no) { dialog, _ -> dialog.dismiss() }
        .show()
}

fun Fragment.showConfirmationDialog(@StringRes messageRes: Int, onConfirm: () -> Unit) {
    MaterialAlertDialogBuilder(requireContext())
        .setMessage(messageRes)
        .setPositiveButton(R.string.action_yes) { _, _ -> onConfirm() }
        .setNegativeButton(R.string.action_no) { dialog, _ -> dialog.dismiss() }
        .show()
}

fun Fragment.showDatePicker(date: Date? = null, onDateSelected: (date: Date) -> Unit) {
    val nonNullDate = date ?: Date()
    val calendar = Calendar.getInstance().apply { time = nonNullDate }
    DatePickerDialog(
        requireContext(),
        DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            val c = Calendar.getInstance().apply {
                this[Calendar.YEAR] = year
                this[Calendar.MONTH] = month
                this[Calendar.DAY_OF_MONTH] = dayOfMonth
            }
            onDateSelected(c.time)
        }, calendar[Calendar.YEAR], calendar[Calendar.MONTH], calendar[Calendar.DAY_OF_MONTH]
    ).show()
}