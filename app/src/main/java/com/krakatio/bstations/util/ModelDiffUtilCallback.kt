package com.krakatio.bstations.util

import androidx.recyclerview.widget.DiffUtil
import com.krakatio.bstations.domain.model.base.Model

class ModelDiffUtilCallback<T : Model> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem.areItemSameWith(newItem)
    }

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem.areContentSameWith(newItem)
    }
}